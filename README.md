# Element Crash Report to E-Mail

This webservice receives a Crash Report sent by [Element Android](https://github.com/vector-im/element-android) and
1. saves the data to `/tmp/element-crash-$date-$random`. As apache2 has usually a private tmp by the systemd unit, the actual directory may be `/tmp/systemd-private-...-apache2.service-.../tmp/`).
2. sends an e-mail to the configured address. The log and the screenshot are uncompressed before sending.

## Dependencies
This application builds on Flask.
The webserver uses WSGI to run the app.

```bash
sudo apt install python3-flask libapache2-mod-wsgi-py3
```

## Webserver

See [Flask's documentation on `mod_wsgi`](https://flask.palletsprojects.com/en/1.1.x/deploying/mod_wsgi/)
for information on the setup.
An example WSGI-file is included in this project.

An apache2 configuration can look like:
```
    # element crash reporter
    WSGIDaemonProcess element-crash threads=2
    WSGIScriptAlias /element-crash /opt/element-crash-to-email/app.wsgi
    <Directory /opt/element-crash-to-email>
        WSGIProcessGroup element-crash
        WSGIApplicationGroup %{GLOBAL}
        Require all granted
    </Directory>
```

## Configuration
Create `/etc/element-crash/config.ini` with the following content:
```ini
[DEFAULT]
mail_from = sending-address@example
mail_to = recipient@example.com
smtp_host = localhost
report_path = /element-crash/
report_url = htps://example.com/element-crash/
```
