#!/usr/bin/python3

import codecs
import configparser
import datetime
import traceback
import gzip
import tempfile
import pprint

from email.message import EmailMessage
from smtplib import SMTP
from pathlib import Path
from flask import Flask, request
from werkzeug.utils import secure_filename

app = Flask(__name__)


CONFIG = configparser.ConfigParser()
CONFIG.read('/etc/element-crash/config.ini')
CONFIG = CONFIG['DEFAULT']


# Apache sends the URL / to the WSGI application?
@app.route('/', methods=['POST'])
@app.route(CONFIG['report_path'], methods=['POST'])
def crash():
    """
    https://github.com/vector-im/element-android/blob/81ef1415dc43d291afc96b53e0dad8bfbc2ccdcf/vector/src/main/java/im/vector/app/features/rageshake/BugReporter.kt
    """
    now = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
    dir = Path(tempfile.mkdtemp(prefix=f'element-crash-{now}-'))
    filesdir = dir / 'files'
    filesdir.mkdir()
    for filename, filedata in request.files.items():
        filedata.save(filesdir / secure_filename(filename))
    with codecs.open(dir / 'formdata.txt', mode='w', encoding='utf-8') as formhandle:
        formhandle.write(pprint.pformat(request.form))

    try:
        msg = EmailMessage()
        msg['Subject'] = 'Element Crash Report'
        try:
            user_id = request.form['user_id']
        except KeyError:
            user_id = 'unknown user'
        msg['From'] = f'"{user_id}" <{CONFIG["mail_from"]}>'
        msg['To'] = CONFIG['mail_to']
        msg.set_content(pprint.pformat(request.form))
        if 'file' in request.files:
            request.files['file'].seek(0)
            msg.add_attachment(gzip.decompress(request.files['file'].read()),
                               maintype='image',
                               subtype='png',
                               filename='screenshot.png')
        if 'compressed-log' in request.files:
            request.files['compressed-log'].seek(0)
            msg.add_attachment(gzip.decompress(request.files['compressed-log'].read()),
                               maintype='text',
                               subtype='plain',
                               filename='log.txt')

        with SMTP(CONFIG['smtp_host']) as smtp:
            smtp.send_message(msg)

    except Exception:
        # if anything fails here, don't bother the client with it
        traceback.print_exc()

    # the client expects this data as JSON and a status code 200
    return {'report_url': CONFIG['report_url']}, 200
